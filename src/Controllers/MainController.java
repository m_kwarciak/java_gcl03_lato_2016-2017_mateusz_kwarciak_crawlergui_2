package Controllers;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.layout.AnchorPane;

import java.io.IOException;

/**
 * Created by Mateusz on 09.04.2017.
 */
public class MainController {

    @FXML
    private Group mainPanel;

    @FXML
    public void initialize()
    {
        loadLogPanel();
    }

    public void loadLogPanel()
    {
        FXMLLoader loader = new FXMLLoader(this.getClass().getResource("/FXML/LogScene.fxml"));
        AnchorPane anchorPane = null;
        try{
            anchorPane = loader.load();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        LogSceneController logSceneController = loader.getController();
        logSceneController.setMainController(this);
        setScene(anchorPane);

    }

    public void setScene(AnchorPane anchorPane)
    {
        mainPanel.getChildren().clear();
        mainPanel.getChildren().add(anchorPane);
    }

}
