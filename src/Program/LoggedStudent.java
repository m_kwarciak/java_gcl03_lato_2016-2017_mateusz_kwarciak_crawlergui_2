package Program;

import java.io.Serializable;

/**
 * Created by Mateusz on 04.05.2017.
 */
public class LoggedStudent extends Student implements Serializable{
    long time;
    Status status;

    public LoggedStudent()
    {

    }

    public LoggedStudent(Student student,long time, Status status)
    {
        this.setFirstName(student.getFirstName());
        this.setLastName(student.getLastName());
        this.setAge(student.getAge());
        this.setMark(student.getMark());
        this.time=time;
        this.status = status;
    }


    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public boolean isVaild()
    {
        return status != null;
    }

    public String toString()
    {
        return getTime() + ": " + getStatus().toString() + "   " + getMark() + "; " + getFirstName() + "; " + getLastName() + "; " + getAge();
    }
    public String toFile()
    {
        return getTime() + ";" + getStatus().toString() + ";" + getMark() + ";" + getFirstName() + ";" + getLastName() + ";" + getAge() + "#";
    }

}
