package Test;

import Loggers.BinaryLogger;
import Loggers.SerializedLogger;
import Program.LoggedStudent;

import java.util.List;

/**
 * Created by Mateusz on 04.05.2017.
 */
public class LoggedStudentLIST__BinaryLogger {
    public static void main(String args[])
    {
        BinaryLogger binaryLogger = new BinaryLogger();
        List<LoggedStudent> list = binaryLogger.listStudents();

        for (LoggedStudent el: list) {
            System.out.println(el.toString());
        }

    }
}
