package Test;

import Loggers.SerializedLogger;
import Program.LoggedStudent;
import Program.Status;
import Program.Student;
import sun.rmi.runtime.Log;

import java.util.List;

/**
 * Created by Mateusz on 04.05.2017.
 */
public class LoggedStudentLIST__SerializedLogger {
    public static void main(String args[])
    {
        SerializedLogger serializedLogger = new SerializedLogger();
        List<LoggedStudent> list = serializedLogger.listStudents("SerializeLogger\\17_05_04_20_15_05_155_SERIALIZE_LOG.ser");

       for (LoggedStudent el: list) {
           System.out.println(el.toString());
       }

    }
}
